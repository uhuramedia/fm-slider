from django.conf import settings
from django.contrib import admin
from django.db import models
import ckeditor
from models import Slider, SliderPortlet
from portlet.admin import PortletAdmin


class SliderAdmin(admin.ModelAdmin):
    def __init__(self, *args, **kwargs):
        super(SliderAdmin, self).__init__(*args, **kwargs)
        self.formfield_overrides = {
            models.TextField: { 'widget': ckeditor.widgets.CKEditorWidget }
        }

class SliderInline(admin.StackedInline):
    model = Slider
    extra = 0

class SliderPortletAdmin(PortletAdmin):
    inlines = [
        SliderInline,
    ]

admin.site.register(Slider, SliderAdmin)
admin.site.register(SliderPortlet, SliderPortletAdmin)
