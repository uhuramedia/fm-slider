$(document).ready(function() {
    $slideshow = $('#slideshow');
    $slideshow.cycle({
        fx: 'fade',
        speed: $slideshow.data('fading_speed'),
        easeOut: 'easeOutExpo',
        easeIn: 'easeOutCirc',
        timeout: $slideshow.data('timeout'),
        pager: '#slider-nav',
        activePagerClass: 'active'
    });
    $('a.slidenav').click(function() {
        var r = $(this).attr('rel');
        $slideshow.cycle(parseInt(r) - 1);
        return false;
    });
    $('#slider-nav a').html('<span></span>');
});
