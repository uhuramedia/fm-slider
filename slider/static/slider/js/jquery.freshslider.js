// jquery.freshslider v 1.1.2

/* TODO:
 *      - add a loading icon and show the slider if it is finaly loaded
 *      - more flow on size changing
 *
 */

(function ($) {

    'use strict';

    $.fn.freshslider = function (options) {
        var i;

        var slideshow = this;
        var slidesInner = slideshow.find('.slides-inner:first');
        var items = slidesInner.children('.slide-item');
        var itemPos = {
            0: 0
        };

        var navTypes = ['arrows', 'bullets', 'both'];
        var animations = [false];
        var animationTimer;
        var active = 0;
        var navBullets;
        var navArrows;

        var defaults = {
            isActiveClass: 'freshslider-active',
            animation: animations[0],
            fadingSpeed: '400',
            timeout: '8000',
            autoSlide: false,
            cycle: false,
            navType: navTypes[0],
            navContainer: slideshow,
            firstSlide: function () { return true; },
            lastSlide: function () { return true; }
        };

        var S = $.extend({}, defaults, options);

        var init = function () {
            // to check is plugin is active on this element
            slideshow.addClass(S.isActiveClass);

            _resize();

            // ARROWS
            if (_isNavArrows()) {
                navArrows = $('<ul class="slider-nav arrows"/>');

                $.each(['prev', 'next'], function (key, name) {
                    navArrows.append($('<li/>').append($('<a/>', {
                        'class': 'arrow ' + name,
                        'title': 'Slide to ' + name,
                        'text': name
                    })));
                });

                S.navContainer.remove('.slider-nav.arrows');
                S.navContainer.append(navArrows);

                var navPrev = navArrows.find('.prev');
                var navNext = navArrows.find('.next');

                _checkPrev(navPrev);
                _checkNext(navNext);

                navPrev.on('click', function () {
                    moveToPrev(navNext, navPrev);
                    setTimer();

                    return false;
                });

                navNext.on('click', function () {
                    moveToNext(navNext, navPrev);
                    setTimer();

                    return false;
                });
            }

            // BULLETS
            if (_isNavBullets()) {
                navBullets = $('<ol class="slider-nav bullets"/>');

                $.each(items, function (index) {
                    var bullet = $('<li><a href="#" class="bullet" title="Slide '
                        + (index + 1)
                        + '">'
                        + (index + 1)
                        + '</a></li>');

                    bullet.on('click', 'a', function () {
                        active = $(this).parent().index();
                        _moveItem();
                        setTimer();

                        return false;
                    });

                    bullet.appendTo(navBullets);
                });

                navBullets.appendTo(slideshow.find('.slider-nav-wrapper:first'));
                _setBulletActive();
            }

            // reset height to prevent ugly page loading
            slidesInner.parent().css('height', 'auto');

            $(window).resize(function () {
                _resize();
            });

            // AUTOSLIDE
            setTimer();
        };

        var timer = function () {
            if (active === items.length - 1) {
                if (S.cycle) {
                    reset();
                } else {
                    clearTimeout(animationTimer);
                    return true;
                }
            } else {
                moveToNext();
            }

            animationTimer = window.setTimeout(timer, S.timeout);
        };

        //set timer - shorthand
        var setTimer = function () {
            // AUTOSLIDE
            if (S.autoSlide) {
                clearTimeout(animationTimer);
                animationTimer = window.setTimeout(timer, S.timeout);
            }
        };

        // slide to the previus item
        var moveToPrev = function (next, prev) {
            _prevItem();

            if (_isNavArrows()) {
                _checkPrev(prev);
                _checkNext(next);
            }
        };

        // slide to the next item
        var moveToNext = function (next, prev) {
            _nextItem();

            if (_isNavArrows()) {
                _checkPrev(prev);
                _checkNext(next);
            }
        };

        var reset = function () {
            active = 0;

            _move(itemPos[0]);

            _setBulletActive();
        };

        var _resize = function () {
            var width = slideshow.width();

            items.width(width);
            slideshow.children('.slides').css('width', width);
            slidesInner.css({
                'width': width * items.length,
                'margin-left': -(width * active)
            });

            // set the start position of each element by getting the width
            for (i = 1; i < items.length; i++) {
                itemPos[i] = $(items[i]).width() + itemPos[i - 1];
            }
        };

        // moves to the previous item, neccessary for position calculation
        var _prevItem = function () {
            if ((active - 1) >= 0) {
                active = active - 1;

                if (active === 0 && typeof S.firstSlide === 'function') {
                    S.firstSlide();
                }
            } else {
                if (S.cycle) {
                    active = items.length - 1;
                } else {
                    active = 0;
                }
            }

            _moveItem();
        };

        // moves to the next item, neccessary for position calculation
        var _nextItem = function () {
            var itemsLength = items.length - 1;
            if ((active + 1) <= itemsLength) {
                active = active + 1;

                if (active === itemsLength && typeof S.lastSlide === 'function') {
                    S.lastSlide();
                }
            } else {
                if (S.cycle) {
                    active = 0;
                } else {
                    active = itemsLength;
                }
            }

            _moveItem();
        };

        // common method to move a item
        var _moveItem = function () {
            _move(-itemPos[active]);

            _setBulletActive();
        };

        var _move = function (movement) {
            // ANIMATION
            if (S.animation) {
                slidesInner.animate(
                    {'marginLeft': movement},
                    S.fadingSpeed,
                    S.animation
                );
            } else {
                slidesInner.css('marginLeft', movement);
            }
        };

        var _checkPrev = function (target) {
            if (active <= 0 && !S.cycle) {
                slideshow.addClass('nav-prev-hidden');
            } else {
                slideshow.removeClass('nav-prev-hidden');
            }
        };

        var _checkNext = function (target) {
            if (active >= (items.length - 1) && !S.cycle) {
                slideshow.addClass('nav-next-hidden');
            } else {
                slideshow.removeClass('nav-next-hidden');
            }
        };

        var _isNavArrows = function () {
            var navType = S.navType;

            if (navType === navTypes[0] || navType === navTypes[2]) {
                return true;
            }

            return false;
        };

        var _isNavBullets = function () {
            var navType = S.navType;

            if (navType === navTypes[1] || navType === navTypes[2]) {
                return true;
            }

            return false;
        };

        var _isNavBoth = function () {
            if (S.navType === navTypes[2]) {
                return true;
            }

            return false;
        };

        var _setBulletActive = function () {
            // set active bullet
            if (_isNavBullets()) {
                var a = navBullets.find('.bullet');
                a.removeClass('active');
                $(a[active]).addClass('active');
            }
        };

        // start the machine...
        init();
    };

})(jQuery);
