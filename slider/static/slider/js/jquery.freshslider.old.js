/* ----------------------------------------------------------------------------
 * TODO:
 *      - add use case for arrows AND bullets
 */

(function ($) {
    'use strict';

    var W = window;
    var D = document;
    var i;

    var M = {
        opt: null,
        navTypes: ['arrows', 'bullets', 'both'],
        animations: [false, 'slideup'],
        animationTimer: null,
        active: 0,

        init: function (options) {
            M.slideshow = $(this);
            M.slidesInner = M.slideshow.find('.slides-inner');
            M.items = M.slidesInner.children();
            M.opt = $.extend({
                animation: M.animations[0],
                fadingSpeed: '400',
                timeout: '8000',
                autoSlide: false,
                cycle: false,
                autoWrapperHeight: true,
                autoWrapperWidth: true,
                sliderHeight: M.items.height(),
                sliderWidth: M.items.width(),
                navType: M.navTypes[0],
                firstSlide: function () { return true; },
                lastSlide: function () { return true; }
            }, options);

            M.itemPos = {
                0: 0
            };

            M._resizeSlider();

            // set the start position of each element by getting the width
            for (i = 1; i < M.items.length; i++) {
                M.itemPos[i] = $(M.items[i]).width() + M.itemPos[i - 1];
            }

            // ARROWS
            if (M._isNavArrows()) {
                M.navArrows = M.slideshow.find('.slider-nav');

                var arrowTags = [];
                var arrowTagNames = ['prev', 'next']
                for (var i = 0; i < arrowTagNames.length; i++) {
                    var arrowTag = D.createElement('a');
                    arrowTag.className = 'arrow ' + arrowTagNames[i];
                    arrowTag.innerHTML = '<span>' + arrowTagNames[i] + '</span>';
                    arrowTags.push(arrowTag);
                }

                M.navArrows.children().remove();
                M.navArrows
                    .append(arrowTags)
                    .addClass('arrows');

                M.navPrev = M.navArrows.find('.prev');
                M.navNext = M.navArrows.find('.next');

                M._checkPrev(M.navPrev);
                M._checkNext(M.navNext);

                $(M.navPrev).bind('click', function () {
                    M.moveToPrev();
                    M.setTimer();

                    return false;
                });

                $(M.navNext).unbind('click').bind('click', function () {
                    M.moveToNext();
                    M.setTimer();

                    return false;
                });
            }

            // BULLETS
            if (M._isNavBullets()) {
                M.navBullets = $('<ol class="slider-nav bullets"></ol>');

                // <div class="slider-nav-wrapper">
                    //
                // </div>

                $.each(M.items, function (index, item) {
                    var bullet = $('<li><a href="#" class="bullet" title="Slide '
                        + (index + 1)
                        + '">'
                        + (index + 1)
                        + '</a></li>');

                    bullet.on('click', 'a', function () {
                        M.active = $(this).parent().index();
                        M._moveItem();
                        M.setTimer();

                        return false;
                    });

                    bullet.appendTo(M.navBullets);
                })

                M.navBullets.appendTo(M.slideshow.find('.slider-nav-wrapper'));
                M._setBulletActive();
            }

            return this.each(function () {
                $(W).resize(function () {
                    M._resizeSlider();
                });

                // AUTOSLIDE
                M.setTimer();
            });
        },
        timer: function () {
            if (M.active === M.items.length - 1) {
                if (M.opt.cycle) {
                    M.reset();
                } else {
                    clearTimeout(M.animationTimer);
                    return true;
                }
            } else {
                M.moveToNext();
            }

            M.animationTimer = W.setTimeout(M.timer, M.opt.timeout);
        },
        //set timer - shorthand
        setTimer: function () {
            // AUTOSLIDE
            if (M.opt.autoSlide) {
                clearTimeout(M.animationTimer);
                M.animationTimer = W.setTimeout(M.timer, M.opt.timeout);
            }
        },
        // slide to the previus item
        moveToPrev: function () {
            M._prevItem();

            if (M._isNavArrows()) {
                M._checkPrev($(M.navPrev));
                M._checkNext($(M.navNext));
            }
        },
        // slide to the next item
        moveToNext: function () {
            M._nextItem();

            if (M._isNavArrows()) {
                M._checkPrev($(M.navPrev));
                M._checkNext($(M.navNext));
            }
        },
        reset: function () {
            M.active = 0;

            // ANIMATION
            if (M.opt.animation) {
                M.slidesInner.animate({
                    'marginLeft': M.itemPos[0]
                }, M.opt.fadingSpeed);
            } else {
                M.slidesInner.css('marginLeft', M.itemPos[0]);
            }

            M._setBulletActive();
        },
        _resizeSlider: function () {
            var $slides = M.slideshow.children('.slides');

            if (M.opt.autoWrapperHeight) {
                $slides.height(M.opt.sliderHeight);
            }
            M.slidesInner.height(M.opt.sliderHeight);

            M.items.width(M.opt.sliderWidth);
            M.slideshow.width(M.opt.sliderWidth);
            if (M.opt.autoWrapperWidth) {
                $slides.width(M.opt.sliderWidth);
            }
            M.slidesInner.width(M.opt.sliderWidth * M.items.length);
        },
        // moves to the previous item, neccessary for position calculation
        _prevItem: function () {
            if ((M.active - 1) >= 0) {
                M.active = M.active - 1;

                if (M.active === 0 && typeof M.opt.firstSlide === 'function') {
                    M.opt.firstSlide();
                }
            } else {
                if (M.opt.cycle) {
                    var itemsLength = M.items.length - 1;
                    M.active = itemsLength - 1;

                    var direction = -(M.slidesInner.width() - $(M.slidesInner[0]).children(':first').width());
                    // ANIMATION
                    if (M.opt.animation) {
                        M.slidesInner.animate({
                            'marginLeft': direction
                        }, M.opt.fadingSpeed);
                    } else {
                        M.slidesInner.css('marginLeft', direction);
                    }

                    M._setBulletActive();
                    return false;
                } else {
                    M.active = 0;
                }
            }

            M._moveItem();
        },
        // moves to the next item, neccessary for position calculation
        _nextItem: function () {
            var itemsLength = M.items.length - 1;
            if ((M.active + 1) <= itemsLength) {
                M.active = M.active + 1;

                if (M.active === itemsLength && typeof M.opt.lastSlide === 'function') {
                    M.opt.lastSlide();
                }
            } else {
                if (M.opt.cycle) {
                    M.reset();
                    return true;
                } else {
                    M.active = itemsLength;
                }
            }

            M._moveItem();
        },
        // common method to move a item
        _moveItem: function () {
            var marginLeft = -M.itemPos[M.active];

            // ANIMATION
            if (M.opt.animation) {
                // if 'slideup'
                if (M.opt.animation === M.animations[1]) {
                    M.slidesInner.animate({
                        'marginLeft': marginLeft
                    }, M.opt.fadingSpeed);
                }
            } else {
                M.slidesInner.css('marginLeft', marginLeft);
            }

            M._setBulletActive();
        },
        _checkPrev: function (target) {
            if (M.active <= 0 && M.opt.cycle === false) {
                target.css('visibility', 'hidden');
            } else {
                target.css('visibility', 'visible');
            }
        },
        _checkNext: function (target) {
            if (M.active >= (M.items.length - 1) && M.opt.cycle === false) {
                target.css('visibility', 'hidden');
            } else {
                target.css('visibility', 'visible');
            }
        },
        _isNavArrows: function () {
            var navType = M.opt.navType;

            if (navType === M.navTypes[0] || navType === M.navTypes[2]) {
                return true;
            }

            return false;
        },
        _isNavBullets: function () {
            var navType = M.opt.navType;

            if (navType === M.navTypes[1] || navType === M.navTypes[2]) {
                return true;
            }

            return false;
        },
        _isNavBoth: function () {
            if (M.opt.navType === M.navTypes[2]) {
                return true;
            }

            return false;
        },
        _setBulletActive: function () {
            // set active bullet
            if (M._isNavBullets()) {
                var a = M.navBullets.find('.bullet');
                a.removeClass('active');
                $(a[M.active]).addClass('active');
            }
        }
    };

    $.fn.freshslider = function (method) {
        if (M[method]) {
            return M[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || ! method) {
            return M.init.apply(this, arguments);
        } else {
            $.error('Method ' +  method + ' does not exist on jQuery.freshslider');
        }
    };

})(jQuery);
