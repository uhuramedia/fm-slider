# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from portlet.models import Portlet

from django.conf import settings


try:
    SLIDER_PORTLET_TEMPLATE = settings.SLIDER_PORTLET_TEMPLATE
except AttributeError:
    SLIDER_PORTLET_TEMPLATE = 'slider/portlet.html'


class Slider(models.Model):
    portlet = models.ForeignKey('SliderPortlet')
    name = models.CharField(max_length=128)
    image = models.ImageField(
        upload_to="sliders",
        width_field="image_height",
        height_field="image_width",
        blank=True,
        null=True
    )
    image_height = models.PositiveIntegerField(blank=True, null=True)
    image_width = models.PositiveIntegerField(blank=True, null=True)
    slider_text = models.TextField('Inhalt', blank=True, null=True)
    sort = models.PositiveSmallIntegerField(_('Sortierung'), default=0)

    def __unicode__(self):
        return "%s %s" % (self.pk, self.name)


class SliderPortlet(Portlet):
    template = SLIDER_PORTLET_TEMPLATE
    fading_speed = models.PositiveIntegerField(
        _('Einblend-geschwindigkeit'),
        default=400
    )
    timeout = models.PositiveIntegerField(
        _('Pause zwischen den Slides'),
        default=8000
    )
    slider_class = models.CharField(
        _('Slider CSS Klasse'),
        max_length=255,
        blank=True,
        null=True
    )

    def slides(self):
        return self.slider_set.order_by('sort')
